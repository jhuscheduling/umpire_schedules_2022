
Gurobi 9.0.2 (linux64, Matlab) logging started Fri 11 Mar 2022 12:30:10 PM EST

Gurobi Optimizer version 9.0.2 build v9.0.2rc0 (linux64)

Concurrent MIP optimizer: 8 concurrent instances (4 threads per instance)

Presolve removed 8 rows and 8 columns
Presolve time: 0.04s
Presolved: 3476 rows, 3660 columns, 16428 nonzeros
Variable types: 0 continuous, 3660 integer (3660 binary)

Root relaxation: objective 0.000000e+00, 880 iterations, 0.14 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0      -    -    -     4.3420e+08    0.00000   100%     -    0s
     0     0      -    -    -     3.4171e+08 1.2051e+07  96.5%     -    1s
     0     0      -    -    -     3.4171e+08 3.5392e+07  89.6%     -    2s
     0     0      -    -    -     3.1153e+08 3.5512e+07  88.6%     -    2s
     0     0      -    -    -     3.1153e+08 4.0414e+07  87.0%     -    3s
     0     0      -    -    -     3.1153e+08 4.0427e+07  87.0%     -    3s
     0     0      -    -    -     3.1153e+08 4.0428e+07  87.0%     -    3s
     0     0      -    -    -     3.1153e+08 4.0429e+07  87.0%     -    3s
     0     0      -    -    -     3.1153e+08 4.0820e+07  86.9%     -    4s
     0     0      -    -    -     3.1153e+08 4.0859e+07  86.9%     -    5s
     0     0      -    -    -     3.1153e+08 4.0866e+07  86.9%     -    5s
     0     0      -    -    -     3.1153e+08 4.0867e+07  86.9%     -    5s
     0     0      -    -    -     3.1153e+08 4.1062e+07  86.8%     -    5s
     0     0      -    -    -     3.1153e+08 5.0859e+07  83.7%     -    6s
     0     0      -    -    -     3.1153e+08 5.0864e+07  83.7%     -    6s
     0     0      -    -    -     3.1153e+08 5.0867e+07  83.7%     -    6s
     0     0      -    -    -     3.1153e+08 5.0888e+07  83.7%     -    7s
     0     0      -    -    -     3.1153e+08 5.0892e+07  83.7%     -    7s
     0     0      -    -    -     3.1153e+08 5.0901e+07  83.7%     -    8s
     0     2      -    -    -     2.0893e+08 5.0918e+07  75.6%     -    9s
     7    12      -    -    -     2.0893e+08 5.0949e+07  75.6%     -   10s
    70    81      -    -    -     9.6277e+07 5.0981e+07  47.0%     -   15s
   129   120      -    -    -     6.5489e+07 5.1224e+07  21.8%     -   16s
   176   148      -    -    -     6.5289e+07 5.1289e+07  21.4%     -   20s
   311   210      -    -    -     6.5289e+07 5.1338e+07  21.4%     -   25s
   383   221      -    -    -     5.6118e+07 5.1370e+07  8.46%     -   30s
   429   224      -    -    -     5.6118e+07 5.1458e+07  8.30%     -   41s
   464   234      -    -    -     5.5269e+07 5.1731e+07  6.40%     -   55s
   556   304      -    -    -     5.5269e+07 5.1887e+07  6.12%     -   62s
   572   303      -    -    -     5.5269e+07 5.1887e+07  6.12%     -   62s
   598   307      -    -    -     5.5269e+07 5.2702e+07  4.64%     -   71s
   628   333      -    -    -     5.5269e+07 5.2881e+07  4.32%     -   75s
   724   403      -    -    -     5.5269e+07 5.2894e+07  4.30%     -   78s
   806   458      -    -    -     5.5269e+07 5.2899e+07  4.29%     -   80s
   955   265      -    -    -     5.5269e+07 5.3623e+07  2.98%     -   85s
   984   262      -    -    -     5.5269e+07 5.3623e+07  2.98%     -   85s
  1287   365      -    -    -     5.5269e+07 5.3857e+07  2.55%     -   90s
  1661   507      -    -    -     5.5269e+07 5.4461e+07  1.46%     -   96s
  1959   582      -    -    -     5.5269e+07 5.4800e+07  0.85%     -  102s

Cutting planes:
  Gomory: 2
  Clique: 2
  MIR: 19
  Flow cover: 35
  Zero half: 44
  RLT: 419

Instance 7 was solved

Explored 2921 nodes (908668 simplex iterations) in 104.40 seconds
Thread count was 4 (of 64 available processors)

Solution count 10: 5.52691e+07 

Optimal solution found (tolerance 1.00e-04)
Best objective 5.526910000000e+07, best bound 5.526910000000e+07, gap 0.0000%
