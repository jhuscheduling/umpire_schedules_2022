function minViols = UmpTransitionTest()
% See how many feasible choices an umpire crew has at each transition
% Shows how many back to backs or tier 3 trips a crew has to take

%% League Setup
% put name of schedule file name
addpath("AA_Central")
full_schedule = importSchedule("2022 AA Central Schedule (1.24.22).xlsm");

% find off days, all star break
[breaksafter, ASB] = getBreaksAfter_Ump(full_schedule);
ASBLength = 3; %Need to do manually for now
% remove off days, all star break
schedule = full_schedule(setdiff(1:length(full_schedule),...
    find(full_schedule(:,1)==0)),:);
% extract nslots and their length
[nslots, ncols] = size(schedule);

ntrans = nslots -1;
nteams = ncols - 1;
ncrews = nteams / 2;
schedule  = schedule(:, 1:nteams);

%Team to Team Travel Distance
mileage = [
%  1   2   3   4   5   6   7   8   9  10
%ARK NWA SPR TUL WCH AMA  CC FRI MID  SA
   0   1   1   1   2   3   3   2   3   3
   1   0   1   1   1   2   3   2   3   3
   1   1   0   1   1   3   3   2   3   3
   1   1   1   0   1   2   3   1   2   2
   2   1   1   1   0   2   3   2   3   3
   3   2   3   2   2   0   3   2   1   2
   3   3   3   3   3   3   0   2   2   1
   2   2   2   1   2   2   2   0   1   2
   3   3   3   2   3   1   2   1   0   1
   3   3   3   2   3   2   1   2   1   0
];

numFeas = inf(ntrans, nteams); % will store how many choices crews have
feasTrips = cell(ntrans, nteams); % will store what those choices are
stadiums = zeros(nslots, ncrews);
for i = 1:nslots
    stadiums(i,:) = unique(schedule(i,:));
end

%% Calculate possible choices
for i = 1:ntrans
    choices = stadiums(i+1,:); % Choices of stadiums to go to next
    for j = 1:nteams
        if sum(schedule(i,:)==j) >0
            teams = find(schedule(i,:) == j); % The indices of the teams playing eachother at j
            followBans = [schedule(i+1,teams(1)), schedule(i+1,teams(2))];
            possible1 = setdiff(choices, followBans);
            % possible1 are the choices that don't follow teams
            possible2 = [];
            for k = 1:length(possible1)
                if mileage(j,possible1(k)) < 3
                    possible2 = [possible2 possible1(k)];
                end
            end
            numFeas(i,j) = length(possible2);
            feasTrips{i,j} = possible2;
        end
    end
end

%% Optimize transitions to find conflicts
minViols = zeros(ntrans, 1);
for i = 1:ntrans
       % Create a simple LP with 25 + 5 variables
       % x_jk = 1 if crew j goes to k next and slack if cannot go anywhere
       req = 0;
       rin = 0;
       Aeq = zeros(ncrews, ncrews^2 + ncrews);
       beq = zeros(ncrews, 1);
       Ain = zeros(ncrews, ncrews^2 + ncrews);
       bin = zeros(ncrews, 1);
       % Each team must go to one of their possible choices with slack
       for j = 1:ncrews
           req = req +1;
           feas = feasTrips{i, stadiums(i,j)};
           for kk = 1:numFeas(i,stadiums(i,j))
               k = find(stadiums(i+1,:)==feas(kk));
               Aeq(req, (j-1)*ncrews + (k-1) + 1) = 1;
           end
           Aeq(req, ncrews^2 + j) = 1;
           beq(req, 1) = 1;
       end
       % Each stadium must have at most 1 crew in next slot
       for k = 1:ncrews
           rin = rin + 1;
           for j = 1:ncrews
               Ain(rin, (j-1)*(ncrews) + (k-1)+1) = 1;
           end
           bin(rin, 1) = 1;
       end
       [~, fval] = intlinprog([zeros(1,(ncrews)^2), ones(1,ncrews)], ...
           1:((ncrews)^2+ ncrews), ...
           Ain, bin, Aeq, beq, ...
           zeros(1,(ncrews)^2 + ncrews), ones(1,(ncrews)^2 + ncrews));
       minViols(i) = fval;
end

minViols = [(1:ntrans)' minViols];

