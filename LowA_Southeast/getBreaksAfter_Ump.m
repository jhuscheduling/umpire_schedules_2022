% Function to create a breaksafter vector given a template
% Goes day-by-day through the template. If the day is an off day,
% then appends the previous slot number to the breaksafter vector.
function [breaksafter, ASB] = getBreaksAfter_Ump(schedule)
ASB = Inf;
breaksafter = [];
[rows,~] = size(schedule);
prevslot = 0;
for r=1:rows
    if schedule(r,1) == 0
        breaksafter = [breaksafter prevslot];
        if schedule(r+1,1)==0
            ASB = prevslot;
        end
    else
        prevslot = prevslot + 1;
    end
end

breaksafter = unique(breaksafter);
end