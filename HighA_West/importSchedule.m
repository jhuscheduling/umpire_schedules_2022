function numsched = importSchedule(workbookFile, sheetName, dataLines, dataColumns)
% Function to read in schedule from MLB-provided Excel
% Input Workbook name, Outputs matrix with stadium assignment in each slot,
% length of slot in last column
% Change team names, input rows below

%% Setup
teams = ["EUG", "EVE", "HIL", "SPO", "TRI", "VAN"];

% If no sheet is specified, read first sheet
if nargin == 1 || isempty(sheetName)
    sheetName = "Raw";
end

% If row start and end points are not specified, define defaults
if nargin <= 2
    start_col = "D";
    % Find last column assuming starting column above
    Alphabet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
    end_col = Alphabet(strfind(Alphabet, start_col)+length(teams) - 1);
    % Define range
    dataLines = [3, 173];
    dataColumns = [start_col, end_col];
end 

data_range = dataColumns(1,1) + dataLines(1, 1) + ":" + dataColumns(1,2) + dataLines(1,2);


%% Set up the Import Options and import the data
opts = spreadsheetImportOptions("NumVariables", length(teams));

% Specify sheet and range
opts.Sheet = sheetName;
opts.DataRange = data_range;

% Specify column names and types
opts.VariableNames = teams;
opts.VariableTypes = repmat("char",1,length(teams));

% Import the data
rawsched = readtable(workbookFile, opts, "UseExcel", false);
rawsched = table2array(rawsched);

% Grab teams and convert to number of which stadium team is playing at
numsched = zeros(size(rawsched));
for i=1:size(rawsched,1)
    for j=1:size(rawsched,2)
        if isempty(rawsched{i,j})
            numsched(i,j) = 0;
        elseif ~contains(rawsched{i,j},'@')
            numsched(i,j) = j;
        else
            visitingteam = char(extractAfter(rawsched{i,j},"@"));
            teamnumber = find(teams == visitingteam);
            numsched(i,j) = teamnumber;
        end
    end
end

% Calculate series lengths and append to last column, remove duplicate rows
numsched(:,end+1)=0;
currow = 1;
while(currow < length(numsched))
    if all(numsched(currow,1:end-1) == 0)
        numsched(currow,end) = 1;
    else
        while(all(numsched(currow,1:end-1) == numsched(currow+1,1:end-1)))
            numsched(currow,end) = numsched(currow,end) + 1;
            numsched(currow+1,:) = [];
            if (currow == length(numsched))
                break                
            end
        end
        numsched(currow,end) = numsched(currow,end) + 1;
    end
    currow = currow + 1;
end

% Remove trailing rows of 0s
while(all(numsched(end, 1:end-1)==0))
   numsched = numsched(1:end-1, :);
end
end