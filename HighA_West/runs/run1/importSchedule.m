function numsched = importSchedule(workbookFile, sheetName, dataLines)
% Function to read in schedule from MLB-provided Excel
% Input Workbook name, Outputs matrix with stadium assignment in each slot,
% length of slot in last column
% Change team names, input rows below

%% Setup
teams = ["EUG"	"EVE"	"HIL"	"SPO"	"TRI"	"VAN"];

% If no sheet is specified, read first sheet
if nargin == 1 || isempty(sheetName)
    sheetName = "Raw";
end

% If row start and end points are not specified, define defaults
if nargin <= 2
    dataLines = [34, 172]; % CHANGE FOR EACH WORKBOOK
end

%% Set up the Import Options and import the data
opts = spreadsheetImportOptions("NumVariables", 10);

% Specify sheet and range
opts.Sheet = sheetName;
opts.DataRange = "D" + dataLines(1, 1) + ":M" + dataLines(1, 2);

% Specify column names and types
opts.VariableNames = teams;
opts.VariableTypes = repmat("char",1,length(teams));

% Import the data
rawsched = readtable(workbookFile, opts, "UseExcel", false);

for idx = 2:size(dataLines, 1)
    opts.DataRange = "D" + dataLines(idx, 1) + ":M" + dataLines(idx, 2);
    tb = readtable(workbookFile, opts, "UseExcel", false);
    rawsched = [rawsched; tb]; 
end
rawsched = table2array(rawsched);

numsched = zeros(size(rawsched));
for i=1:size(rawsched,1)
    for j=1:size(rawsched,2)
        if isempty(rawsched{i,j})
            numsched(i,j) = 0;
        elseif ~contains(rawsched{i,j},'@')
            numsched(i,j) = j;
        else
            visitingteam = char(extractAfter(rawsched{i,j},"@"));
            teamnumber = find(teams == visitingteam);
            numsched(i,j) = teamnumber;
        end
    end
end

numsched(:,end+1)=0;
currow = 1;
while(currow < length(numsched))
    if all(numsched(currow,1:end-1) == 0)
        numsched(currow,end) = 1;
    else
        while(all(numsched(currow,1:end-1) == numsched(currow+1,1:end-1)))
            numsched(currow,end) = numsched(currow,end) + 1;
            numsched(currow+1,:) = [];
            if (currow == length(numsched))
                break                
            end
        end
        numsched(currow,end) = numsched(currow,end) + 1;
    end
    currow = currow + 1;
end
end