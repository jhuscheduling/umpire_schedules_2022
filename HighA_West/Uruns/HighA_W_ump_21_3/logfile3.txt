
Gurobi 8.1.1 (linux64, Matlab) logging started Fri Mar 26 11:29:53 2021

Optimize a model with 1389 rows, 1449 columns and 5391 nonzeros
Variable types: 0 continuous, 1449 integer (1449 binary)
Coefficient statistics:
  Matrix range     [1e+00, 1e+00]
  Objective range  [1e+00, 5e+03]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 2e+00]

Concurrent MIP optimizer: 8 concurrent instances (10 threads per instance)

Found heuristic solution: objective 208130.00000
Presolve removed 165 rows and 165 columns
Presolve time: 0.01s
Presolved: 1224 rows, 1284 columns, 4896 nonzeros
Variable types: 0 continuous, 1284 integer (1284 binary)

Root relaxation: objective 0.000000e+00, 313 iterations, 0.02 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0      -    -    -     208130.000    0.00000   100%     -    0s
     0     0      -    -    -     80126.0000 5346.66667  93.3%     -    0s
     0     0      -    -    -     30140.0000 13138.6667  56.4%     -    0s
     0     0      -    -    -     30140.0000 13479.3333  55.3%     -    0s
     0     0      -    -    -     30140.0000 13569.5000  55.0%     -    0s
     0     0      -    -    -     30140.0000 13896.1667  53.9%     -    0s
     0     0      -    -    -     30140.0000 13897.1667  53.9%     -    0s
     0     0      -    -    -     30140.0000 19244.8542  36.1%     -    0s
     0     0      -    -    -     30140.0000 19503.7300  35.3%     -    0s
     0     0      -    -    -     30140.0000 19633.3889  34.9%     -    0s
     0     0      -    -    -     30140.0000 19795.6667  34.3%     -    0s
     0     0      -    -    -     30140.0000 19795.6667  34.3%     -    0s
     0     0      -    -    -     30140.0000 21835.7789  27.6%     -    0s
     0     0      -    -    -     30140.0000 22129.0938  26.6%     -    0s
     0     0      -    -    -     30140.0000 22146.5163  26.5%     -    0s
     0     0      -    -    -     30140.0000 22160.9499  26.5%     -    0s
     0     0      -    -    -     30140.0000 22160.9587  26.5%     -    0s
     0     0      -    -    -     30140.0000 24959.2152  17.2%     -    0s
     0     0      -    -    -     30140.0000 25256.2356  16.2%     -    0s
     0     0      -    -    -     30140.0000 25294.9491  16.1%     -    0s
     0     0      -    -    -     30140.0000 25297.5382  16.1%     -    0s
     0     0      -    -    -     30140.0000 26874.3232  10.8%     -    0s

Instance 0 was solved

Explored 1 nodes (2470 simplex iterations) in 0.89 seconds
Thread count was 10 (of 160 available processors)

Solution count 10: 30140 30140 30140 ... 71141 

Optimal solution found (tolerance 1.00e-04)
Best objective 3.014000000000e+04, best bound 3.014000000000e+04, gap 0.0000%
