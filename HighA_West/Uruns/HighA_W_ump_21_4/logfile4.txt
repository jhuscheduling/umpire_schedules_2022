
Gurobi 8.1.1 (linux64, Matlab) logging started Mon Apr 26 13:49:45 2021

Optimize a model with 1485 rows, 1545 columns and 6360 nonzeros
Variable types: 0 continuous, 1545 integer (1545 binary)
Coefficient statistics:
  Matrix range     [1e+00, 1e+00]
  Objective range  [1e+02, 5e+03]
  Bounds range     [1e+00, 1e+00]
  RHS range        [1e+00, 2e+00]

Concurrent MIP optimizer: 8 concurrent instances (10 threads per instance)


MIP start did not produce a new incumbent solution
MIP start violates constraint R1389 by 6.000000000

Found heuristic solution: objective 187600.00000
Presolve time: 0.01s
Presolved: 1485 rows, 1545 columns, 6360 nonzeros
Variable types: 0 continuous, 1545 integer (1545 binary)

Root relaxation: objective 0.000000e+00, 329 iterations, 0.04 seconds

    Nodes    |    Current Node    |     Objective Bounds      |     Work
 Expl Unexpl |  Obj  Depth IntInf | Incumbent    BestBd   Gap | It/Node Time

     0     0      -    -    -     187600.000    0.00000   100%     -    1s
     0     0      -    -    -     187600.000 5719.99944  97.0%     -    1s
     0     0      -    -    -     164600.000 14004.4742  91.5%     -    1s
     0     0      -    -    -     164600.000 14300.2351  91.3%     -    1s
     0     0      -    -    -     164600.000 14300.2351  91.3%     -    1s
     0     0      -    -    -     147800.000 18638.2028  87.4%     -    2s
     0     0      -    -    -     92700.0000 19666.6667  78.8%     -    2s
     0     0      -    -    -     92700.0000 19733.3333  78.7%     -    2s
     0     0      -    -    -     92700.0000 19900.0000  78.5%     -    2s
     0     0      -    -    -     92700.0000 23136.6667  75.0%     -    2s
     0     0      -    -    -     92700.0000 23241.0569  74.9%     -    2s
     0     0      -    -    -     92700.0000 23415.3333  74.7%     -    2s
     0     0      -    -    -     73100.0000 26275.9815  64.1%     -    3s
     0     0      -    -    -     73100.0000 26753.9216  63.4%     -    3s
     0     0      -    -    -     73100.0000 26886.6667  63.2%     -    3s
     0     0      -    -    -     73100.0000 29413.7579  59.8%     -    3s
     0     2      -    -    -     73100.0000 29413.7579  59.8%     -    4s
     7    16      -    -    -     64000.0000 33581.0185  47.5%     -    5s
    99    69      -    -    -     64000.0000 43087.9747  32.7%     -   11s
   174    96      -    -    -     64000.0000 51170.9947  20.0%     -   15s

Cutting planes:
  Gomory: 3
  MIR: 24
  Zero half: 161

Instance 1 was solved

Explored 332 nodes (43744 simplex iterations) in 20.55 seconds
Thread count was 10 (of 160 available processors)

Solution count 10: 64000 73100 73100 ... 76900 

Optimal solution found (tolerance 1.00e-04)
Best objective 6.400000000000e+04, best bound 6.400000000000e+04, gap 0.0000%
