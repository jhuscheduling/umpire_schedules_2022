function [ByCrew,ExcelFormat,crew_mileage, nprobs,problist] = umpire_AAAEast(xstart)
%% League Setup
addpath('/usr/local/gurobi/linux64/matlab')

% Make sure you have a folder called Uruns, and that you've edited 
% get_run_num_Ump appropriately
%runnum=get_run_num_Ump();

%league_name = 'AAA_East'; %% CHANGE FOR YOUR LEAGUE
%directory = ['Uruns/' league_name '_ump_21_' num2str(runnum)];
%mkdir(directory);  

%League Size
nteams=20;  %% CHANGE FOR YOUR LEAGUE
ncrews=nteams/2;
nstadiums = ncrews;

% Indexing: i = crew, j = stadium index 1 to nteams/2 in given slot, k = slot
% Stadium index not same as stadium number, index re-numbers the stadiums
% that have home games in a given slot
function idx = umpIDX(crew,stadium,slot)
    idx = nstadiums*nslots*(crew-1)+nslots*(stadium-1)+slot;
end

% Mileage bounds - user input
% mileage_upper = 8200;
% mileage_lower = 7800;

% Bookended Series - Want to keep crews with home team
b2bseries = [];

% Team to Team Travel Distance
% From team on Row, to Team on Column
mileage = [
% 1    2    3    4    5    6    7    8    9   10   11   12   13   14   15   16   17   18   19   20
% BUF LHV  ROC  SWB  SYR  WOR  COL  IND  IOW  LOU  OMA  STP  TOL  CHA  DUR  GWN  JAX  MEM  NAS  NOR

  0   197  226  519  491  615  571  691  686  696  855  762  235  432  623  400  200  500  315  217
 197   0   178  469  371  471  435  640  543  560  783  591  321  543  125  350  140  320  224  531
 226  178    0  294  321  398  399  466  454  497  638  569  564  765  432  632  321  132  352  253
 519  469  294    0  244  256  255  174  294  361  479  426  432  345  224  265  346  276  254  186
 491  371  321  244    0  103   66  417  174  191  396  255  764  642  701  412  275  300  199  437
 615  471  398  256  103    0   66  362   79  118  323  215  426  465  345  326  341  244  238  249
 571  435  399  255   66   66    0  420  138  128  328  191  265  257  452  436  231  328  372  180
 691  640  466  174  417  362  420    0  287  339  339  458  354  463  653  222  145  294  385  222
 686  543  454  294  174   79  138  287    0   60  215  183  453  452  722  156  176  241  286  436
 696  560  497  361  191  118  128  339   60    0  208  110  275  445  325  185  230  352  321  111
 855  783  638  479  396  323  328  339  215  208    0  214  435  631  192  264  415  411  123  542
 762  591  569  426  255  215  191  458  183  110  214    0  621  724  410  341  316  286  445  209
 235  321  564  432  764  426  265  354  453  275  435  621   0   309  334  444  177  331  273  157
 432  543  765  345  642  465  257  463  452  445  631  724  309   0   187  354  572  259  197  337
 623  125  432  224  701  345  452  653  722  325  192  410  334  187   0   112  111  167  133  462
 400  350  632  265  412  326  436  222  156  185  264  341  444  354  112   0   362  263  329  395
 200  140  321  346  275  341  231  145  176  230  415  316  177  572  111  362   0   610  274  601
 500  320  132  276  300  244  328  294  241  352  411  286  331  259  167  263  610   0   472  338
 315  224  352  254  199  238  372  385  286  321  123  445  273  197  133  329  274  472   0   624
 217  531  253  186  437  249  180  222  436  111  542  209  157  337  462  395  601  338  624   0
 ];

%% Constraint Declarations
% Need to keep slack on for C9 and C13 in order to use xstart
% use just 1-6, 11, 15 for now
%                  1    2	3	4   5	6	7   8	9	10	11	12	13	14  15  
whichconstraints1= [1	1	1	1   1	1	0   0   0	0	1   0   0	0   1    %on/off
                   0    0   0   1   1   1	1   0   1	1	1   1   1	1   1    %is slack  
                   0	0   0   5e3 1e3	1e2	1e1 3e1 5e2	5e1 1e3 1e3 1e4 1e4 1e5]; %punishment
%                  16   17  18
whichconstraints2 =[0   0   0
                    1   1   1
                    1e5 1e5 1e3];
                
whichconstraints = [whichconstraints1 whichconstraints2]
% Constraint 1: Each crew must be at exactly one stadium per slot
% Constraint 2: There must be one crew working at every game
% Constraint 3: Each crew sees each team at least twice
% Constraint 4: A Crew cannot work two series in a row with the same team
% Constraint 5: Crew can't see the same team 3 out of 4 series
% Constraint 6: Maximum of 32 games of crew with team
% Constraint 7: No trips over 535 miles w/o offday
% Constraint 8: No 600 mile travel
% Constraint 9: Maximum of 2 499 plus trips per crew without offday
% Constraint 10: No two consecutive 500 mile travels
% Constraint 11: No crew should see a team for less than 1 series at home
% Constraint 12: No crew should see a team for less than 1 series away
% Constraint 13: Maximum and minimum mileage for all crews
% Constraint 14: Hardwire slots for umpire crews stay with home team
% Constraint 15: Crew can't see the same team 2 out of 3 series
% Constraint 16: Each crew should be assigned to two series in Daytona
% Constraint 17: Each crew should be assigned to four series combined at Jupiter and Palm Beach.  
% Constraint 18: Travel tiers

% put name of schedule file name
full_schedule = importSchedule("2021 AAA-East w Last Time Teams Play Each Other-Delayed start.xlsx");

% find off days, all star break
[breaksafter, ASB] = getBreaksAfter_Ump(full_schedule);
% remove off days, all star break
schedule = full_schedule(setdiff(1:length(full_schedule),...
    find(full_schedule(:,1)==0)),:);
% extract slots and their length
[nslots,~] = size(schedule);
gamesperslot = schedule(:,end);
% remove last column
schedule = schedule(:,1:end-1);
asbLength = 0; % input length of all star break, 0 if no break

% make matrix that is 1 if team is home in given slot, 0 otherwise
hometeam = zeros(size(schedule));
% make matrix that lists the indices of stadiums that are hosting games in each slot 
homestadiums = zeros(nslots,nstadiums);
for i = 1:nslots
    for j = 1:nteams
        hometeam(i,j) = schedule(i,j) == j;
    end
    homestadiums(i,:) = find(hometeam(i,:));
end

%% Matrix Setup
numconstrainttypes=length(whichconstraints);
howmanyeq=zeros(1,numconstrainttypes);
howmanyineq=zeros(1,numconstrainttypes);
howmanycolumns=zeros(1,numconstrainttypes);

rin=0;                                   % index of row for Ain
req=0;                                   % index of row for Aeq
cols=nslots*ncrews*nstadiums;            % index of columns for Ain/Aeq

cushionrows=10000;
cushioncolumns=10000;

Ain=spalloc(cushionrows,cols+cushioncolumns,8*cushionrows);   % storing inequality constraints
Aeq=spalloc(cushionrows,cols+cushioncolumns,8*cushionrows);   % storing equality constraints
bin=zeros(cushionrows,1);
beq=zeros(cushionrows,1);
f=zeros(cols+cushioncolumns,1);

TRin=zeros(cushionrows,4);  % will keep track of constrainttype, slot, team, stadium indices for the rows
TReq=zeros(cushionrows,4);
xslack=zeros(cols+cushioncolumns,1); % xstart for slacks

completed = 0; %keeps track of how many constraints have been set up

%% Constraint 1: Each crew must be in exactly 1 stadium per slot
if whichconstraints(1,1) == 1
    for i = 1:ncrews
        for k = 1:nslots
            req = req+1;
            for j = 1:nstadiums
                Aeq(req, umpIDX(i,j,k)) = 1;
            end
            beq(req) = 1;
        end
    end
end
completed=completed+1
howmanyeq(completed)=req;
howmanyineq(completed)=rin;
howmanycolumns(completed)=cols;

%% Constraint 2: There must be one crew working at every game
if whichconstraints(1,2) == 1
    for k = 1:nslots
        for j = 1:nstadiums
            req = req + 1;
            for i = 1:ncrews
                Aeq(req, umpIDX(i,j,k)) = 1;
            end
            beq(req) = 1;
        end
    end
end
completed=completed+1
howmanyeq(completed)=req;
howmanyineq(completed)=rin;
howmanycolumns(completed)=cols;

%% Constraint 3: minimum 2 series with every crew
if whichconstraints(1,3) == 1
    for i = 1:ncrews
        for t = 1:nteams
            rin = rin + 1;
            for k = 1:nslots
                j = find(homestadiums(k,:)==schedule(k,t));
                Ain(rin, umpIDX(i,j,k)) = -1;
            end
            bin(rin) = -2;
            TRin(rin,:)=[3 i inf t];
            if whichconstraints(2,3)==1
                cols = cols + 1;
                Ain(rin,cols) = -1;
                f(cols,1) = whichconstraints(3,3);
                xslack(cols,1) = 1;
            end
        end
    end
end
completed=completed+1
howmanyeq(completed)=req;
howmanyineq(completed)=rin;
howmanycolumns(completed)=cols;

%% Constraint 4: A Crew cannot work two series in a row with the same team
if whichconstraints(1,4) == 1
    for i = 1:ncrews
        for t = 1:nteams
            for k = 1:(nslots-1)
                stad0 = find(homestadiums(k,:)==schedule(k,t));
                stad1 = find(homestadiums(k+1,:)==schedule(k+1,t));
                
                rin = rin + 1;
                Ain(rin, umpIDX(i,stad0,k)) = 1;
                Ain(rin, umpIDX(i,stad1,k+1)) = 1;
                bin(rin) = 1;
                TRin(rin,:)=[4 i k t];
                if whichconstraints(2,4)==1
                    cols = cols + 1;
                    Ain(rin,cols) = -1;
                    f(cols,1) = whichconstraints(3,4);
                    xslack(cols,1) = 1;
                end
            end
        end
    end
end
completed=completed+1
howmanyeq(completed)=req;
howmanyineq(completed)=rin;
howmanycolumns(completed)=cols;

%% Constraint 5: crew can't see the same team 3 out of 4 series.
% crew can't see same team 2 out of 3
if whichconstraints(1,5) == 1
    for i = 1:ncrews
        for t = 1:nteams
            for k = 1:(nslots-3)
                stad0 = find(homestadiums(k,:)==schedule(k,t));
                stad1 = find(homestadiums(k+1,:)==schedule(k+1,t));
                stad2 = find(homestadiums(k+2,:)==schedule(k+2,t));
                stad3 = find(homestadiums(k+3,:)==schedule(k+3,t));

                rin = rin + 1;
                Ain(rin, umpIDX(i,stad0,k)) = 1;
                Ain(rin, umpIDX(i,stad1,k+1)) = 1;
                Ain(rin, umpIDX(i,stad2,k+2)) = 1;
                Ain(rin, umpIDX(i,stad3,k+3)) = 1;
                bin(rin) = 2;
                TRin(rin,:)=[5 i k t];
                if whichconstraints(2,5)==1
                    cols = cols + 1;
                    Ain(rin,cols) = -1;
                    f(cols,1) = whichconstraints(3,5);
                    xslack(cols,1) = 1;
                end
            end
        end
    end
end
completed=completed+1
howmanyeq(completed)=req;
howmanyineq(completed)=rin;
howmanycolumns(completed)=cols;

%% Constraint 6: maximum of 30 games of crew with team
if whichconstraints(1,6) == 1
    for i = 1:ncrews
        for t = 1:nteams
            rin = rin + 1;
            for k = 1:nslots
                j = find(homestadiums(k,:)==schedule(k,t));
                Ain(rin, umpIDX(i,j,k)) = gamesperslot(k);
            end
            bin(rin) = 30;
            TRin(rin,:)=[6 i inf t];
            if whichconstraints(2,6)==1
                cols = cols + 1;
                Ain(rin,cols) = -1;
                f(cols,1) = whichconstraints(3,6);
                xslack(cols,1) = 1;
            end
        end
    end
end
completed=completed+1
howmanyeq(completed)=req;
howmanyineq(completed)=rin;
howmanycolumns(completed)=cols;

%% Constraint 7: no trips over 535 miles w/o offday
if whichconstraints(1,7) == 1
    for i = 1:ncrews
        for k = setdiff(1:nslots-1,breaksafter)
            for t = homestadiums(k,:)
                for tt = homestadiums(k+1,:)
                    if mileage(t,tt) >= 535
                        stad0 = find(homestadiums(k,:)==t);
                        stad1 = find(homestadiums(k+1,:)==tt);

                        rin = rin + 1;
                        Ain(rin, umpIDX(i,stad0,k)) = 1;
                        Ain(rin, umpIDX(i,stad1,k+1)) = 1;
                        bin(rin) = 1;
                        TRin(rin,:)=[7 i k t];
                        if whichconstraints(2,7)==1
                            cols = cols + 1;
                            Ain(rin,cols) = -1;
                            f(cols,1) = whichconstraints(3,7);
                            xslack(cols,1) = 1;
                        end
                    end
                end
            end
        end
    end
end
completed=completed+1
howmanyeq(completed)=req;
howmanyineq(completed)=rin;
howmanycolumns(completed)=cols;

%% Constraint 8: no 600 mile travel
if whichconstraints(1,8) == 1
    for i = 1:ncrews
        for k = 1:nslots-1
            for t = homestadiums(k,:)
                for tt = homestadiums(k+1,:)
                    if mileage(t,tt) >= 600
                        stad0 = find(homestadiums(k,:)==t);
                        stad1 = find(homestadiums(k+1,:)==tt);

                        rin = rin + 1;
                        Ain(rin, umpIDX(i,stad0,k)) = 1;
                        Ain(rin, umpIDX(i,stad1,k+1)) = 1;
                        bin(rin) = 1;
                        TRin(rin,:)=[8 i k t];
                        if whichconstraints(2,8)==1
                            cols = cols + 1;
                            Ain(rin,cols) = -1;
                            f(cols,1) = whichconstraints(3,8);
                            xslack(cols,1) = 1;
                        end
                    end
                end
            end
        end
    end
end
completed=completed+1
howmanyeq(completed)=req;
howmanyineq(completed)=rin;
howmanycolumns(completed)=cols;

%% Constraint 9: maximum of 2 499 plus trips per crew without offday
if whichconstraints(1,9) == 1
    for i=1:ncrews
        indicators = [];
        for k = setdiff(1:nslots-1,breaksafter)
            for t = homestadiums(k,:)
                for tt = homestadiums(k+1,:)
                    if mileage(t,tt)<535 || mileage(t,tt)>591
                        continue
                    end
                    stad0 = find(homestadiums(k,:)==t);
                    stad1 = find(homestadiums(k+1,:)==tt);
                    
                    rin = rin + 1;
                    Ain(rin, umpIDX(i,stad0,k)) = 1;
                    Ain(rin, umpIDX(i,stad1,k+1)) = 1;
                    cols = cols + 1;
                    Ain(rin,cols) = -1;
                    xslack(cols,1) = 1;
                    indicators = [indicators cols];
                    bin(rin) = 1;
					TRin(rin,:) = [9 i k t];
                end
            end
        end
        rin = rin + 1;
        for ind = indicators
            Ain(rin,ind) = 1;
        end
        bin(rin) = 2;
		TRin(rin,:) = [9 i inf inf];
		if whichconstraints(2,9)==1
        	cols = cols + 1;
            Ain(rin,cols) = -100;
            f(cols,1) = whichconstraints(3,9);
            xslack(cols,1) = 1;
        end
    end
end
completed=completed+1
howmanyeq(completed)=req;
howmanyineq(completed)=rin;
howmanycolumns(completed)=cols;

%% Constraint 10: no two consecutive 500 mile travels
if whichconstraints(1,10) == 1
    for i = 1:ncrews
        for k = 1:(nslots-2)
            for t = homestadiums(k,:)
                for tt = homestadiums(k+1,:)
                    for ttt = homestadiums(k+2,:)
                        if mileage(t,tt)>=500 && mileage(tt,ttt)>=500
                            stad0 = find(homestadiums(k,:)==t);
                            stad1 = find(homestadiums(k+1,:)==tt);
                            stad2 = find(homestadiums(k+2,:)==ttt);
                            
                            rin = rin + 1;
                            Ain(rin, umpIDX(i,stad0,k)) = 1;
                            Ain(rin, umpIDX(i,stad1,k+1)) = 1;
                            Ain(rin, umpIDX(i,stad2,k+2)) = 1;
                            bin(rin) = 2;
                            TRin(rin,:)=[10 i k t];
                            if whichconstraints(2,10)==1
                                cols = cols + 1;
                                Ain(rin,cols) = -1;
                                f(cols,1) = whichconstraints(3,10);
                                xslack(cols,1) = 1;
                            end
                        end
                    end
                end
            end
        end
    end
end
completed=completed+1
howmanyeq(completed)=req;
howmanyineq(completed)=rin;
howmanycolumns(completed)=cols;

%% Constraint 11: No crew should see a team for less than 1 series at home
if whichconstraints(1,11) == 1
    for i = 1:ncrews
        for t = 1:nteams
            rin = rin + 1;
            for k = 1:nslots
                j = find(homestadiums(k,:)==schedule(k,t));
                if homestadiums(k,j)==t
                    Ain(rin, umpIDX(i,j,k)) = -1;
                end
            end
            bin(rin) = -1;
            TRin(rin,:)=[11 i inf t];
            if whichconstraints(2,11)==1
                cols = cols + 1;
                Ain(rin,cols) = -1;
                f(cols,1) = whichconstraints(3,11);
                xslack(cols,1) = 1;
            end
        end
    end
end
completed=completed+1
howmanyeq(completed)=req;
howmanyineq(completed)=rin;
howmanycolumns(completed)=cols;

%% Constraint 12: No crew should see a team for less than 1 series away
if whichconstraints(1,12) == 1
    for i = 1:ncrews
        for t = 1:nteams
            rin = rin + 1;
            for k = 1:nslots
                j = find(homestadiums(k,:)==schedule(k,t));
                if homestadiums(k,j)~=t
                    Ain(rin, umpIDX(i,j,k)) = -1;
                end
            end
            bin(rin) = -1;
            TRin(rin,:)=[12 i inf t];
            if whichconstraints(2,12)==1
                cols = cols + 1;
                Ain(rin,cols) = -1;
                f(cols,1) = whichconstraints(3,12);
                xslack(cols,1) = 1;
            end
        end
    end
end
completed=completed+1
howmanyeq(completed)=req;
howmanyineq(completed)=rin;
howmanycolumns(completed)=cols;

%% Constraint 13: Maximum and minimum mileage
if whichconstraints(1,13) == 1
    for i = 1:ncrews
        indicators = zeros(2, nslots*nteams/2*nteams/2);
        ind_column = 0;
        for k = 1:nslots - 1
            for t = homestadiums(k,:)
                for tt = homestadiums(k+1,:)
                    stad0 = find(homestadiums(k,:)==t);
                    stad1 = find(homestadiums(k+1,:)==tt);
                    
                    rin = rin + 1;
                    Ain(rin, umpIDX(i,stad0,k)) = 1;
                    Ain(rin, umpIDX(i,stad1,k+1)) = 1;
                    bin(rin) = 1;
                    
                    cols = cols + 1;
                    Ain(rin,cols) = -1;
                    xslack(cols,1) = 1;
                    
                    ind_column = ind_column + 1;
                    indicators(:,ind_column) = [cols mileage(t,tt)];
                    TRin(rin,:) = [13.1 k t tt];
                end
            end
        end
        
        indicators = indicators(:,1:ind_column);
        rin = rin + 1;
        for ind = indicators
            Ain(rin, ind(1)) = ind(2);
        end
        bin(rin) = mileage_upper;
        TRin(rin,:) = [13 i inf 1];
        if whichconstraints(2,13)==1
            cols = cols + 1;
            Ain(rin,cols) = -sum(mileage,'all')*(nslots-1);
            f(cols,1) = 1e3*whichconstraints(3,13);
            xslack(cols,1) = 1;
        end
        
        rin = rin + 1;
        for ind = indicators
            Ain(rin, ind(1)) = -ind(2);
        end
        bin(rin) = -mileage_lower;
        TRin(rin,:) = [13 i inf 2];
        if whichconstraints(2,13)==1
            cols = cols + 1;
            Ain(rin,cols) = -sum(mileage,'all')*(nslots-1);
            f(cols,1) = 1e3*whichconstraints(3,13);
            xslack(cols,1) = 1;
        end
        
        % Makes sure that each crew only make trip between series
        req = req + 1;
        for ind = indicators
            Aeq(req, ind(1)) = 1;
        end
        beq(req) = nslots - 1;
		TReq(req,:) = [13 i inf nslots-1];
		if whichconstraints(2,13)==1
			cols = cols + 1;
			Aeq(req,cols) = -(nslots-1)*(ncrews*nstadiums - 1);
			f(cols,1) = whichconstraints(3,13);
			xslack(cols,1) = 1;
		end
    end
end

completed=completed+1
howmanyeq(completed)=req;
howmanyineq(completed)=rin;
howmanycolumns(completed)=cols;

%% Constraint 14: Hardwire bookended slots umpire crews stay with home team
if whichconstraints(1,14) == 1
    for i = 1:ncrews 
        for k = b2bseries
            for t = homestadiums(k,:)
                stad0 = find(homestadiums(k,:)==schedule(k,t));
                stad1 = find(homestadiums(k+1,:)==schedule(k+1,t));
                
                rin = rin + 1;
                Ain(rin, umpIDX(i,stad0,k)) = -1;
                Ain(rin, umpIDX(i,stad1,k+1)) = 1;
                bin(rin) = 0;
                TRin(rin,:)=[14 i k t];
                if whichconstraints(2,14)==1
                    cols = cols + 1;
                    Ain(rin,cols) = -1;
                    f(cols,1) = whichconstraints(3,14);
                    xslack(cols,1) = 1;
                end
            end
        end
    end
end
completed=completed+1
howmanyeq(completed)=req;
howmanyineq(completed)=rin;
howmanycolumns(completed)=cols;

%% Constraint 15: No crew can see a team for more than 20 games at home
if whichconstraints(1,15) == 1
    for i = 1:ncrews
        for t = 1:nteams
            for k = 1:(nslots-2)
                stad0 = find(homestadiums(k,:)==schedule(k,t));
                stad1 = find(homestadiums(k+1,:)==schedule(k+1,t));
                stad2 = find(homestadiums(k+2,:)==schedule(k+2,t));
                
                rin = rin + 1;
                Ain(rin, umpIDX(i,stad0,k)) = 1;
                Ain(rin, umpIDX(i,stad1,k+1)) = 1;
                Ain(rin, umpIDX(i,stad2,k+2)) = 1;
                bin(rin) = 1;
                TRin(rin,:)=[5 i k t];
                if whichconstraints(2,5)==1
                    cols = cols + 1;
                    Ain(rin,cols) = -1;
                    f(cols,1) = whichconstraints(3,5);
                    xslack(cols,1) = 1;
                end
            end
        end
    end
end
completed=completed+1
howmanyeq(completed)=req;
howmanyineq(completed)=rin;
howmanycolumns(completed)=cols;

%% Constraint 16: Each crew should visit Daytona twice
if whichconstraints(1,11) == 1
    DAY = 7;
    for i = 1:ncrews
        rin = rin + 1;
        for k = 1:nslots
            j = find(homestadiums(k,:)==DAY);
            if ~isempty(j)
                Ain(rin, umpIDX(i,j,k)) = -1;
            end
        end
        bin(rin) = -2;
        TRin(rin,:)=[16 i inf t];
        if whichconstraints(2,16)==1
            cols = cols + 1;
            Ain(rin,cols) = -1;
            f(cols,1) = whichconstraints(3,16);
            xslack(cols,1) = 1;
        end
    end
end
completed=completed+1
howmanyeq(completed)=req;
howmanyineq(completed)=rin;
howmanycolumns(completed)=cols;

%% Constraint 17: Each crew should be assigned to four series combined at Jupiter and Palm Beach 
if whichconstraints(1,17) == 1
    JUP = 8;
    PMB = 9;
    for i = 1:ncrews
        rin = rin + 1;
        for k = 1:nslots
            for t = [JUP PMB]
                j = find(homestadiums(k,:)==t);
                if ~isempty(j)
                    Ain(rin, umpIDX(i,j,k)) = -1;
                end
            end
        end
        bin(rin) = -4;
        TRin(rin,:)=[17 i inf t];
        if whichconstraints(2,17)==1
            cols = cols + 1;
            Ain(rin,cols) = -1;
            f(cols,1) = whichconstraints(3,17);
            xslack(cols,1) = 1;
        end
    end
end
completed=completed+1
howmanyeq(completed)=req;
howmanyineq(completed)=rin;
howmanycolumns(completed)=cols;

%% Constraint 18: Travel tiers
if whichconstraints(1,18) == 1
    for i = 1:ncrews
        for k = 1:nslots-1
            for t = homestadiums(k,:)
                for tt = homestadiums(k+1,:)
                    traveltier = mileage(t,tt);
                    if traveltier > 1
                        stad0 = find(homestadiums(k,:)==t);
                        stad1 = find(homestadiums(k+1,:)==tt);

                        rin = rin + 1;
                        Ain(rin, umpIDX(i,stad0,k)) = 1;
                        Ain(rin, umpIDX(i,stad1,k+1)) = 1;
                        bin(rin) = 1;
                        TRin(rin,:)=[18 i k traveltier];
                        if whichconstraints(2,7)==1
                            cols = cols + 1;
                            Ain(rin,cols) = -1;
                            switch traveltier
                                case 2
                                    f(cols,1) = whichconstraints(3,7) / 10;
                                case 3
                                    f(cols,1) = whichconstraints(3,7);
                            end
                            xslack(cols,1) = 1;
                        end
                    end
                end
            end
        end
    end
end
completed=completed+1
howmanyeq(completed)=req;
howmanyineq(completed)=rin;
howmanycolumns(completed)=cols;

%% Matrix Work
for i=completed:-1:2
    howmanyeq(i)=howmanyeq(i)-howmanyeq(i-1);
    howmanyineq(i)=howmanyineq(i)-howmanyineq(i-1);
    howmanycolumns(i)=howmanycolumns(i)-howmanycolumns(i-1);
end
howmanycolumns(1)=howmanycolumns(1)-nslots*ncrews*nstadiums;

manyeq=howmanyeq
manyineq=howmanyineq
manycolumns=howmanycolumns
numberofvariables=cols

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

Aeq=Aeq(1:req,1:cols);
Ain=Ain(1:rin,1:cols);
beq=beq(1:req,1);
bin=bin(1:rin,1);
f=f(1:cols,1);

TRin=TRin(1:rin,:);
TReq=TReq(1:req,:);

%% Gurobi
newA = [Aeq; Ain];
newb = [beq; bin];
newA = sparse(newA);
sense_eq = repmat('=',req,1);
sense_in = repmat('<',rin,1);
sense = [sense_eq; sense_in];
vtype = repmat('B',cols,1);
model.A = newA;
model.obj = f;
model.modelsense = 'min';
model.rhs = newb;
model.sense = sense;
model.vtype = vtype;
if length(xstart)>2
    rr=nslots*ncrews*nstadiums;
    xstart=[xstart(1:rr); xslack(rr+1:cols)];
    model.start = xstart;
end

params.Threads=80;
params.ConcurrentMIP=8 ;
params.MIPFocus = 1;
params.LogFile = [directory '/logfile' num2str(runnum) '.txt'];
disp(params)
result = gurobi(model,params);

%% Get Results
result.status

%If infeasible, try to find which constraints are causing issues
if strcmp(result.status,'INFEASIBLE')
    const_key = zeros(req+rin,1);
    counter = 1;
    for c = 1:completed
        const_key(counter:counter+manyeq(c)) = c;
        counter = counter+manyeq(c);
    end
    for c = 1:completed
        const_key(counter:counter+manyineq(c)) = c;
        counter = counter+manyineq(c);
    end
    disp("Possible Infeasible Constraints:");
    iis = gurobi_iis(model,params);
    disp(const_key(iis.Arows==1));
    ByCrew=[];
    ExcelFormat=[];
    crew_mileage=[];
    nprobs=[];
    problist =[];
    return
end


x = result.x;
x=round(x);

csvwrite([directory '/x.csv'], x);

AinTruncate=Ain(:, 1:(nslots*nstadiums*ncrews)  );
AeqTruncate=Aeq(:, 1:(nslots*nstadiums*ncrews)  );
xTruncate=x( 1:(nslots*nstadiums*ncrews)   );
probin= AinTruncate*xTruncate > bin ;
probeq=AeqTruncate*xTruncate ~= beq ;
% remove constraints that only rely on combinations of slack variables, since 
% they will automatically show up in the problist after truncating Ain, Aeq
probinlist=TRin(setdiff(find(probin), find(sum(AinTruncate,2)==0)),:); 
probeqlist=TReq(setdiff(find(probeq), find(sum(AeqTruncate,2)==0)),:); 
problist=[probinlist; probeqlist];
problist=problist(problist(:,1)~=13.1,:); %remove constraints to track travel in c13
nprobs=zeros(1,length(whichconstraints));
for i=1:length(whichconstraints)
  nprobs(i)=sum( problist(:,1) == i );
end

ByCrew = zeros(nslots,ncrews);
for i = 1:ncrews
    for j = 1:nstadiums
        for k = 1:nslots
            if x(umpIDX(i,j,k)) == 1
                ByCrew(k,i) = homestadiums(k,j);
            end
        end
    end
end

series = 1;
game = 0;
ExcelFormat = zeros(sum(gamesperslot) + length(breaksafter)+asbLength,ncrews);
while (series <= nslots)
    for i=1:(gamesperslot(series))
        game = game + 1;
        ExcelFormat(game,:)=ByCrew(series,:);
    end
    if (series == ASB)
        for i=1:asbLength
            game = game + 1;
            ExcelFormat(game,:)=zeros(1,ncrews);
        end
    elseif (any(breaksafter == series)) && (series ~= ASB)
        game = game + 1;
        ExcelFormat(game,:)= zeros(1,ncrews);
    end
    series = series + 1; 
end

crew_mileage = zeros(ncrews,1);
for i = 1:ncrews
    for k = 1:nslots-1
        crew_mileage(i) = crew_mileage(i) + mileage(ByCrew(k,i),ByCrew(k+1,i));
    end
end

csvwrite([directory '/ByCrew.csv'], ByCrew);
csvwrite([directory '/ExcelFormat.csv'], ExcelFormat);
csvwrite([directory '/nprobs.csv'], nprobs);
%writetable(nprobs,'Uruns/nprobsbymonth.csv','Delimiter',',','QuoteStrings',true)
csvwrite([directory '/problist.csv'], problist);

teamschedule = [];
sched=full_schedule(:,1:end-1);
gamesperslot = full_schedule(:,end);
for i=1:length(sched)
    if ~all(sched(i,1:nteams)==0)
        for j = 1:nteams
            if sched(i,j) ~= j
                sched(i,j) = -sched(i,j);
            else
                sched(i,j) = setdiff(find(abs(sched(i,:))==j),j);
            end
        end
    end
    
    teamschedule = [teamschedule; repmat(sched(i,:),gamesperslot(i),1)];
end
csvwrite([directory '/teamschedule.csv'], teamschedule);

end
